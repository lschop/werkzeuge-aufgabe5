# Wie kompliert man ein Programm?

1. Sicherstellen, dass man im richtigen Ordner drin ist und auf dem richtigen Branche
2. Mit Hilfe des _javac_ Befehls soll die Datei/ das Programm kompiliert werden. Das könnte dann aussehen wie folgt:
<pre> javac HelloWorld.java </pre>
3. War das Kompilieren erfolgreich, wird nichts ausgegeben. Sonst wird eine Fehlermeldung angegeben
4. Erst bei der Ausführung des Programmes wird etwas(hoffentlich das gewünschte) ausgeben.  
Ausführung erfolgt mittels _java_ Befehl wie folgt:
<pre> java HelloWorld </pre>  
Bei unserem Programm sollte dann im ideal Fall "Moin Moin" auf der Console angezeigt werden
